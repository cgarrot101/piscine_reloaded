/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strdup.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: cgarrot <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/02 17:35:41 by cgarrot      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/02 18:00:58 by cgarrot     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strdup(char *src)
{
	int		a;
	char	*c;
	int		b;

	a = 0;
	b = 0;
	while (src[a] != '\0')
		a++;
	if (!(c = (char*)malloc(sizeof(char) * a)))
		return (0);
	while (src[b] != '\0')
	{
		c[b] = src[b];
		b++;
	}
	c[b] = '\0';
	return (c);
}
