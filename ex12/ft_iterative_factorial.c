/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_iterative_factorial.c                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: cgarrot <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/01 17:01:09 by cgarrot      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/02 21:38:12 by cgarrot     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

int	ft_iterative_factorial(int nb)
{
	int a;
	int b;

	b = nb;
	a = nb;
	if (0 < nb && nb < 13)
	{
		while (a != 1)
		{
			nb = b * (a - 1);
			b = nb;
			a--;
		}
		return (b);
	}
	else if (nb > 12 || nb < 0)
		return (0);
	else if (nb == 0 || nb == 1)
		return (1);
	return (0);
}
