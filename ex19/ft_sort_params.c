/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_sort_params.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: cgarrot <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/02 10:30:00 by cgarrot      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/02 22:05:25 by cgarrot     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putstr(char *str)
{
	int		i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
	ft_putchar('\n');
}

int		ft_strcmp(char *s1, char *s2)
{
	int		i;

	i = 0;
	while (s1[i] != '\0')
	{
		if ((s1[i] + '0' < s2[i] + '0') || (s1[i] + '0' > s2[i] + '0'))
			return (s1[i] - s2[i]);
		i++;
	}
	return (0);
}

int		main(int ac, char **av)
{
	int		i;
	int		j;
	char	*tmp;

	j = ac;
	while (j > 0)
	{
		i = ac - 1;
		while (i > 0)
		{
			if ((ft_strcmp(av[i], av[i - 1]) < 0) && (av[i - 1] != av[0]))
			{
				tmp = av[i - 1];
				av[i - 1] = av[i];
				av[i] = tmp;
			}
			i--;
		}
		j--;
	}
	i = 1;
	while (av[i])
		ft_putstr(av[i++]);
	return (0);
}
