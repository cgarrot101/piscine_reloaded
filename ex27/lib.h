/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lib.h                                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: cgarrot <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/02 19:53:34 by cgarrot      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/02 19:55:22 by cgarrot     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef LIB_H
# define LIB_H
# define DIV "Stop : division by zero\n"
# define MOD "Stop : modulo by zero\n"
# define BUF_SIZE 4096

void	ft_putchar(char c);
void	ft_putstr(char *str);
int		ft_atoi(char *str);
void	ft_putnbr(int nb);
void	ft_putchar2(char c);
void	ft_putstr2(char *str);

#endif
