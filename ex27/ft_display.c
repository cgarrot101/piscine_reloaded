/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_display.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: cgarrot <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/02 19:50:01 by cgarrot      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/02 19:51:38 by cgarrot     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "lib.h"

int	error(int ac)
{
	if (ac < 2)
	{
		ft_putstr2("File name missing.\n");
		return (0);
	}
	if (ac > 2)
	{
		ft_putstr2("Too many arguments.\n");
		return (0);
	}
	return (1);
}

int	main(int ac, char **av)
{
	int		fd;
	int		red;
	char	buf[BUF_SIZE + 1];

	fd = open(av[1], O_RDONLY);
	if (error(ac) == 0)
		return (0);
	if (fd == -1)
	{
		ft_putstr2("open() error\n");
		return (1);
	}
	red = read(fd, buf, BUF_SIZE);
	buf[red] = '\0';
	ft_putstr(buf);
	if (close(fd) == -1)
	{
		ft_putstr2("close() error");
		return (1);
	}
	return (0);
}
